<?php 
namespace Task;


class JsonFormat implements FormatInterface
{
    public function decode($products)
    {
        return json_decode($products);
    }
}
?>